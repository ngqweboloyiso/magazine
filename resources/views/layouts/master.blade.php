<!doctype html>
<html>
<head>

  @include('includes.head')
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <link href="/public/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="/public/css/bootstrap.css" rel="stylesheet" type="text/css">
  <link href="/public/css/app.css" rel="stylesheet" type="text/css">
  <style>
    html, body {
      background-color: #ffffff;
      font-family: 'Raleway', sans-serif;
      font-weight: 100;
      height: 100vh;
      margin: 0;
    }


    .position-ref {
      position: relative;
    }


    .content {
      text-align: center;
    }

    .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }


    .title {
      font-size: 84px;
      border-top: 1px solid black;
      border-bottom: 1px solid black;
      text-align: center;
      margin: 60px;
    }

    .nav > a {
      padding: 0 25px;
      font-size: 12px;
      font-weight: 600;
      letter-spacing: .1rem;
      text-decoration: none;
      text-transform: uppercase;
      background-color: #ffffff;
      
    }
    
    .nav {
      text-align: center;
      display: block;
      padding: 8px 16px;
    }

    .m-b-md {
      margin-bottom: 60px;
      margin-top:60px;
    }
    
    /* div.gallery img {
      width: 100%;
      height: auto;
    } */

    div.desc {
      padding: 5px;
      text-align: center;
    }

    div.gallery {
        width: 640px;
        margin: 0 auto;
        padding: 10px;
        background: #fff;
        position: relative;
}

div.gallery img {
  display: block;

  transition: .1s transform;
  transform: translateZ(0); /* hack */
}

    .responsive {
      padding: 0 6px;
      float: left;
      width: 24%;
    }

 
  </style>
</head>

<body>


  <div class="container">
   
    <header class="row">
      @include('includes.header')
      
    </header>

    <div class="row">
      
      @include('includes.gallery')
      
    </div>

    <div id="main" class="row" class="col-md-8">
      
      @yield('content')
      
    </div>
    
    <footer class="row">
      @include('includes.footer')
    </footer>
    
  </div>

</body>

</html>
